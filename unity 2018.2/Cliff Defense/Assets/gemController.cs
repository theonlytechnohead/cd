﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gemController : MonoBehaviour
{

	Animator animator;

	// Use this for initialization
	void Start()
	{
		animator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void flashGem()
	{
		animator.SetTrigger("flash");
	}
}
