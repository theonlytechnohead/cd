﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cameraScript : MonoBehaviour {

	Camera mainCamera;
	public Scrollbar scrollbar;
	public Vector3 topPos;
	public Vector3 bottomPos;

	// Use this for initialization
	void Start()
	{
		mainCamera = GetComponent<Camera>();
	}

	// Update is called once per frame
	void Update()
	{
		Vector3 pos = transform.position;
		pos = Vector3.Lerp(bottomPos, topPos, scrollbar.value);
		transform.position = pos;
	}
}
